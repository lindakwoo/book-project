from django.shortcuts import render, get_object_or_404
from .models import Book


# def books_list(request):
#     books_list = Book.objects.all()  # this is built into Django
#     context = {
#         "books": books_list,  # the key name is used in the html file
#         "heading": "My list of books"
#     }

#     return render(request, "books/book_list.html", context)

def books_list(request, id):
    books_list = Book.objects.all()  # this is built into Django
    print('request', request)
    book = get_object_or_404(Book, id=id)
    # book = Book.objects.get(id=id)
    context = {
        "books": books_list,  # the key name is used in the html file
        "heading": "My list of books",
        "book": book
    }

    return render(request, "books/book_list.html", context)
