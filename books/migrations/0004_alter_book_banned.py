# Generated by Django 5.0.6 on 2024-05-31 20:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0003_book_banned'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='banned',
            field=models.BooleanField(null=True),
        ),
    ]
