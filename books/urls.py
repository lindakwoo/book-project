from django.urls import path
from .views import books_list

urlpatterns = [
    path('books', books_list, name="books_list"),
    # looks for an integer in the url path
    path('books/<int:id>/', books_list, name='books')
]
