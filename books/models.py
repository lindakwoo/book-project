from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    year_published = models.SmallIntegerField()
    publisher_name = models.CharField(max_length=100, null=True)
    banned = models.BooleanField(default=False)

    def __str__(self) -> str:  # prints to the admin page
        return self.title
